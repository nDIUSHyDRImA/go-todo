package task

import "testing"

func TestNewでidが正しく入る(t *testing.T) {
	arr := []string{"foo", "bar", "baz"}
	for index, c := range arr {
		got := NewTask(c)
		if index+1 != got.Id() {
			t.Error()
		}
	}
}

func TestNewでtextが正しく入る(t *testing.T) {
	cases := []string{"foo", "bar", "baz"}
	for _, c := range cases {
		got := NewTask(c)
		if got.Text() != c {
			t.Error()
		}
	}
}

func TestToggleCompleteでcompletedが変わる(t *testing.T) {
	task := NewTask("foo")
	if task.IsCompleted() {
		t.Error()
	}
	task.ToggleComplete()
	if !task.IsCompleted() {
		t.Error()
	}
	task.ToggleComplete()
	if task.IsCompleted() {
		t.Error()
	}
}
