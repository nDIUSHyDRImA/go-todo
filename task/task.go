package task

import "encoding/json"

var id = 1

type Task struct {
	id          int
	text        string
	isCompleted bool
}

func (task Task) Id() int {
	return task.id
}

func (task Task) Text() string {
	return task.text
}

func (task Task) IsCompleted() bool {
	return task.isCompleted
}

func (task *Task) ToggleComplete() {
	task.isCompleted = !task.isCompleted
}

func NewTask(text string) *Task {
	task := Task{id: id, text: text}
	id += 1
	return &task
}

func (task *Task) EditText(text string) {
	task.text = text
}

func (task *Task) MarshalJSON() ([]byte, error) {
	v, err := json.Marshal(&struct {
		Id          int    `json:"id"`
		Text        string `json:"text"`
		IsCompleted bool   `json:"isCompleted"`
	}{Id: task.id, Text: task.text, IsCompleted: task.isCompleted})
	return v, err
}
