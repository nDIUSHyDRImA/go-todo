package main

import (
	"encoding/json"
	"net/http"
	"strconv"

	taskPack "todo/task"

	"github.com/gin-gonic/gin"
)

var tasks = []taskPack.Task{*taskPack.NewTask("Go学ぶ"), *taskPack.NewTask("Docker学ぶ"), *taskPack.NewTask("NestJS学ぶ")}

func main() {
	router := gin.Default()
	router.GET("/tasks", getTasks)
	router.POST("/complete/:id", completeTask)
	router.POST("/delete/:id", deleteTask)
	router.POST("/edit/:id", editTask)
	router.POST("/add", addTask)

	router.Run("localhost:8080")
}

func getTasks(c *gin.Context) {
	tasksJson, _ := json.Marshal(tasks)
	c.IndentedJSON(http.StatusOK, string(tasksJson))
}

func completeTask(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	for index, task := range tasks {
		if task.Id() == id {
			tasks[index].ToggleComplete()
			taskJson, _ := json.Marshal(&tasks[index])
			c.IndentedJSON(http.StatusOK, string(taskJson))
			return
		}
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Not Found"})
}

func deleteTask(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	for index, task := range tasks {
		if task.Id() == id {
			tasks = append(tasks[:index], tasks[index+1:]...)
			c.IndentedJSON(http.StatusOK, gin.H{"message": "Deleted"})
			return
		}
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Not Found"})
}

type newTaskJsonStruct struct {
	Text string
}

func addTask(c *gin.Context) {
	var newTaskJson newTaskJsonStruct
	c.BindJSON(&newTaskJson)
	if newTaskJson.Text == "" {
		c.IndentedJSON(http.StatusUnprocessableEntity, gin.H{"message": "Empty Text"})
	}
	newTask := taskPack.NewTask(newTaskJson.Text)
	tasks = append(tasks, *newTask)
	taskJson, _ := json.Marshal(&newTask)
	c.IndentedJSON(http.StatusOK, string(taskJson))
}

type editTaskJsonStruct struct {
	Text string
}

func editTask(c *gin.Context) {
	var editTaskJson editTaskJsonStruct
	c.BindJSON(&editTaskJson)
	if editTaskJson.Text == "" {
		c.IndentedJSON(http.StatusUnprocessableEntity, gin.H{"message": "Empty Text"})
	}
	id, _ := strconv.Atoi(c.Param("id"))
	for index, task := range tasks {
		if task.Id() == id {
			tasks[index].EditText(editTaskJson.Text)
			taskJson, _ := json.Marshal(&tasks[index])
			c.IndentedJSON(http.StatusOK, string(taskJson))
			return
		}
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Not Found"})
}
